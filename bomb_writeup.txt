Name: Alexander Samuels, SPC
Date:  26MAY2016
Current Module: Intel x86 Assembly
Project Name:  Bomb

Project Goals:
	The goal of this project is to use all availiable tools and knowledge to create a write-up describing what the execution of bomb.

Analysis of Strings:
	Strings of interest that popped out were:
		7D5v
		getenv
		syscall
		dH34%(
		Nice try, but such a stage does not exist.UH
		Stage 1:
		Stage 2:
		Stage 3:
		Stage 4:
		Stage 5:
		Stage 6:
		Stage 7:
		Stage 8:
		Stage ?:
		swordfish
		USER
		%u %u %u %u %u
		%c %d %c
		%d %c %d
		?%d %c %d
		stage_8_input.txt
		20151010
		hidden_message
		filename
		hidden_phrase
		stage_secret
		stage_super_duper_secret
		name_length
		sscanf
		fgets
		strlen
		strcmp
	Most notable strings and why:
		swordfish - A passphrase for a previous exercise.
		getenv/USER - After handing out the project, Liam mentioned that the answers from one person could not simply be plugged into another's. With this knowledge, one could deduce that the program would take some measures to ensure this was the case, using the $USER could be a way for this to enforced.
		%u %u %u %u %u/%c %d %c/ %d %c %d - Common way to denote the format of a string being read or written in the printf family of function calls.
		Stage ?: - There could be more than just eight stages?
		sscanf/fgets/strlen/strcmp - Curious function calls that have been overwritten in previous exercises and could be overwritten/modified in this project.

Common passphrases after examining strings:
	1234
	12345
	qwerty
	asamuels
	password
	swordfish

STAGE 1:
	Passphrase: swordfish

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		Classmates

	My findings:
		First and foremost, upon trying to start the program in GDB, it seg faulted. My findings for this stage were clearly focused on the output of strings ran against the entire bomb executable. Various strings of interest popped out, as shown above, one of the most notably being 'swordfish'. This was also the passphrase for a previous GDB exercise designed to do the same thing as this project. The passphrase was also in my list of common passphrases ran against each stage.

	Resources found:
		Classmates are helpful. And Liam can be helpful, sometimes, when he isn't lying.
	Ideas from people:
		I believe someone shouted out that the passphrase was in the output of strings which only furthered investigation on the output.
	Ideas from Liam:
		After handing out the assignment, Liam wrote on the board various programs on the board and reccommended use of them. The first he wrote was strings. The passphrase was in the output of running strings against the program.
	Ideas from Internet:
		N/A

STAGE 2:
	Passphrase: asamuels

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings

	My findings:
		Upon looking at stage_2 function in objdump, immediately "#602128 <username>" becomes of interest below a call to getenv. The second line of interest is a strcmp function call right before the function returns. This hinted at the passphrase being dependant somehow on the username of the current user. With this knowledge in mind, I went back and tried my current username, asamuels, as the passphrase and it was successful in defusing that current stage. Upon further investigation of the arguments passed to strcmp, it is comparing our input and a constant string stored at 0x602128, and when viewed in GDB, is our username.

	Resources found:
		N/A
	Ideas from people:
		Use objdump as GDB required was not working without additional set up.
	Ideas from Liam:
		objdump was one of the recommended programs for use written on the board.
	Ideas from Internet:
		N/A

STAGE 3:
	Passphrase: 12345

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		Google Chrome
		Classmates

	My findings:
		When I looked at the objdump, I saw a call to strlen and then after that, a static memory address was loaded into esi and coming from the previous problem, the thought that the passphrase was another string came to mind and I began trying suspicious looking strings, the first being the number 20151010, which worked. This satisfied me until after I figured out four, and looked harder at what actually was happening. The username is being concatenated onto whatever input we enter and the string length is calculated and stored off for later. The static memory address when viewed from GDB was in fact a format string used in an sscanf call. The number extracted from sscanf gets divided by the string length. Then it gets fast divided by three and compared against the string length. If it's greater than the string length, then the stage is won, if not, the stage is lost.

	Resources found:
		0xAAAAAAAB
		http://www.icodeguru.com/Embedded/Hacker's-Delight/070.htm
	Ideas from people:
		sbartholomew figured out how to get GDB to work which was a major help in walking through the guts of this function.
	Ideas from Liam:
		A paper was passed out during class reiterating how the fast inverse square root function used in quake was made possible by a seemingly random number.
	Ideas from Internet:
		http://www.icodeguru.com/Embedded/Hacker's-Delight/070.htm
		This site housed two magic numbers, both used in quick unsigned divisions by either a three or a nine. Seeing the magic number corresponding with a quick division by 3 lead me to try inputs that are only multiples of three.

STAGE 4:
	Passphrase: 100 200 400 800 1602

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		jmclaurin
		Google Chrome

	My findings:
		Upon first glance into the function, there was a ton of assigning going on, and jumps to seemingly random positions. This spaghetti structure made only one thing viable, gdb. Upon starting gdb, I attempted to print out any constants that looked like memory locations using both the x/* and p/* family of commands. Doing this, I discovered that a constant passed into the sscanf call containg the string "%u %u %u %u %u" hinted that this function took five numbers seperated by spaces, this string was also shown in the output of running strings against the program. When stepping through gdb, the first letter of the username was set into a memory location and each number was added into this memory location. The only requirement was that each number must be greater than the number currently in the memory location. If this condition is not met, an exit occurs and you do not proceed. After all the numbers are added together, fast division by 7 occurs using the magic number 0x24924925 and the modulo of that is checked. If the number modulo seven is equal to zero, you pass the stage. If there was a remainder, you don't proceed to the next stage.

	Resources found:
		0x24924925:
		http://www.icodeguru.com/Embedded/Hacker's-Delight/070.htm
		https://gnu.googlesource.com/gcc/+/trunk/gcc/hash-table.c
	Ideas from people:
		Collaborated with jmclaurin when walking through this function in gdb. Various ideas were bounced around and sanity checks were made.
	Ideas from Liam:
		Liam reccommended using GDB, which made the disassembly of this function possible.
	Ideas from Internet:
		There was another seeming random number in the code loaded into a register followed by bit-shifting which was much too large to be a memory address. This immediately provoked thoughts of use of another fast division. Upon googling this 0x24924925, various results had this magic number used in unsigned division by seven.

STAGE 5:
	Passphrase: d 1259 d

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		rfollensbee
		sparadais
		jmclaurin
		Google Chrome

	My findings:
		I glanced at the objdump for this function, and with my head still heavy from meticiously combing through stage 4, I fired up gdb. I then proceeded to print constant values and found the string "%c %d %c". I overheard a classmate pointing out that d is equal to 100 and would be a nice value for calculations and seeing what was going on. And so I used the letter d for each of my %c's and randomly chose 5000 for my %d as it was a nice round number. I then ran through gdb and checked what the return value was. I got the number 3586 returned in eax which was a difference of 1526. This is the number I entered on the second iteration. This returned a lower number and by suggestion from rfollensbee and sparadais, I took the difference and came up with the number 1259. This worked and advanced the stage.

	Resources found:
		GDB is now my tool of choice.
	Ideas from people:
		I recieved the suggestion from jmclaurin to use the letter 'd' as it's ascii value is an even one hundred. Another idea/suggestion came from rfollensbee and sparadais to run the program twice, changing each time dependent upon the return value of the previous run.
	Ideas from Liam:
		N/A
	Ideas from Internet:
		N/A


STAGE 6:
	Passphrase: 48 b 48

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		Google Chrome
		Classmates

	My findings:
		Upon entering the function in gdb, I saw that there was yet another sscanf call and printed the memory address where the format should be stored. It printed out the string "%d %c %d". Upon seeing this, it could be deduced that this is probably the format required in the input, as had been for the rpeivous stages as well. Stepping through gdb, there is a call to a function "___". Rather than stepping into this function, I see that it takes the two numbers passed in as arguments and returns the ascii value of the first letter in the username. It then adds the two numbers passed in and checks if it is equal to the character -2.

	Resources found:
		High chance peers are smarter than you.
	Ideas from people:
		sbartholomew helped guide me to the correct passphrase using my username.
	Ideas from Liam:
		N/A
	Ideas from Internet:
		N/A

STAGE 7:
	Passphrase: <Enter>

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		Classmates
		Google Chrome

	My findings:
		Various conversations were had about just having to press enter for stage seven, curiousity took over and I tried it with success. When walked through with gdb, there are random and redundant assigns and reassigns. There is some reading of some numbers from a username label, which is not utilized and ultimately just checks if the value in eax is equal to an rbp-0x8, a register that had zero moved into it.
	Resources found:
		GDB pocket reference combined with the internet is extremely useful.
	Ideas from people:
		There was talk of a stage that required nothing to be inputted, and I have to say, I was not suprised that a empty string was a valid answer for at least one of these stages.
	Ideas from Liam:
		The input is usually the unexpected. Not a direct quote, yet implied.
	Ideas from Internet:
		Different ways to display values at both memory locations and registers were discovered via the internet.

STAGE 8:
	Not reached.
	Passphrase:

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings
		Google Chrome

	My findings:
		Looking at the objdump of stage 8, there are multiple systemcalls with the code zero, along with some mallocs and frees. There is three systemcalls with the code before mentioned.
	Resources found:
		N/A
	Ideas from people:
		N/A
	Ideas from Liam:
		N/A
	Ideas from Internet:
		N/A
STAGE ?:
	Not reached.
	Passphrase:

	Tools used:
		GDB
		objdump -D -M intel bomb
		strings

	My findings:
		N/A
	Resources found:
		N/A
	Ideas from people:
		N/A
	Ideas from Liam:
		N/A
	Ideas from Internet:
		N/A

